# RUN locally

This backend service runs on port 3001 and accessed by woo-tag-video-ui

git clone git clone https://manju16832003@bitbucket.org/wootagtest/woo-tag-video-upload-service.git

`cd woo-tag-video-upload-service`

`npm install`

`node index.js`

## Run Tests

`node test`

Go to http://localhost:3001

or keep an eye on console logs for any errors.

